# Oh, the emptiness

There is nothing here yet. Fill me with meaning.

## What is going to happen now

1. Pull the repo content
2. Change something
3. Push the changes

So, I added a paragraph. How do I commit these changes now?

Done. Added this other paragraph with typing errors that have been fixed.
